﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper.Configuration;

namespace CsvHelperDemo
{
    public class Foo
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class FooMap : ClassMap<Foo>
    {
        public FooMap()
        {
            Map(m => m.Id).Index(0).Name("id");
            Map(m => m.Name).Index(1).Name("name");
        }
    }
}
