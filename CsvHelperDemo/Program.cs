﻿// See https://aka.ms/new-console-template for more information

using System.Globalization;
using System.Text;
using CsvHelper;
using CsvHelper.Configuration;
using CsvHelperDemo;

var records = new List<Foo>
{
    new Foo { Id = 1, Name = "Первый" },
    new Foo { Id = 2, Name = "Второй" },
};
var csvConfig = new CsvConfiguration(CultureInfo.CurrentCulture)
{
    Delimiter = "\t",
    NewLine = Environment.NewLine
};

using (Stream stream = new FileStream("test.csv", FileMode.Create))
using (var writer = new StreamWriter(stream, Encoding.Unicode))
using (var csv = new CsvWriter(writer, csvConfig))
{
    csv.WriteHeader<Foo>();
    csv.NextRecord();
    foreach (var record in records)
    {
        csv.WriteRecord(record);
        csv.NextRecord();
    }
}

var anonRecords = new List<object>
{
    new { Id = 1, Name = "Первый" },
};

using (Stream stream = new FileStream("anon_test.csv", FileMode.Create))
using (var writer = new StreamWriter(stream, Encoding.Unicode))
using (var csv = new CsvWriter(writer, csvConfig))
{
    csv.WriteRecords(anonRecords);
}