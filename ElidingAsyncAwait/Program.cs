﻿namespace ElidingAsyncAwait
{
    internal class Program
    {
        static HttpClient _httpClient = new HttpClient();

        static async Task Main(string[] args)
        {
            Console.WriteLine("___1___ Ожидание прикреплено к таске"); // Одинаковое поведение
            try
            {
                var result1 = await GetWithKeywordsAsync(); // works fine
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine(e);
            }

            try
            {
                var result2 = await GetElidingKeywordsAsync(); // works fine
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine(e);
            }

            Console.WriteLine("___2___ Ожидание и таска разделены"); //  Разное поведение
            var task3 = GetWithKeywordsAsync(); // здесь исключение в результате выполнения метода записывается в task3
            try
            {
                var result3 = await task3; // здесь исключение перехватывается
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine(e);
            }

            Task<string> task4 = null;
            try
            {
                task4 = GetElidingKeywordsAsync(); // здесь исключение на записывается в task4, а выбрасывается напрямую
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine(e);
            }
            try
            {
                var result4 = await task4; // task4 осталась null
            }
            catch (NullReferenceException e)
            {
                Console.WriteLine(e);
            }
        }

        public static async Task<string> GetWithKeywordsAsync()
        {
            Console.WriteLine(">>> GetWithKeywordsAsync");
            string url = "";
            return await _httpClient.GetStringAsync(url);
        }

        public static Task<string> GetElidingKeywordsAsync()
        {
            Console.WriteLine(">>> GetElidingKeywordsAsync");
            string url = "";
            return _httpClient.GetStringAsync(url);
        }
    }
}