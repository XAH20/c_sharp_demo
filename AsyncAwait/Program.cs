﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleTestApp
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine($"Start Main - {Thread.CurrentThread.ManagedThreadId} - {SynchronizationContext.Current?.GetHashCode()}");

            await f1();
            await f3();

            Console.WriteLine($"End Main - {Thread.CurrentThread.ManagedThreadId} - {SynchronizationContext.Current?.GetHashCode()}");

            //Console.ReadKey();
        }


        static async Task f1()
        {
            Console.WriteLine($"Task 1 START - {Thread.CurrentThread.ManagedThreadId}");
            //Console.WriteLine($"Task 1 WAIT - {Thread.CurrentThread.ManagedThreadId}");
            //f2().Wait();

            await Task.Delay(1000);

            Console.WriteLine($"Task 1 END - {Thread.CurrentThread.ManagedThreadId}");

        }

        static async Task f2()
        {
            Console.WriteLine($"Task 2 START - {Thread.CurrentThread.ManagedThreadId}");

            await Task.Delay(10000);

            Console.WriteLine($"Task 2 END - {Thread.CurrentThread.ManagedThreadId}");

        }

        static async Task f3()
        {
            Console.WriteLine($"Task 3 START - {Thread.CurrentThread.ManagedThreadId}");

            await Task.Delay(1000);

            Console.WriteLine($"Task 3 END - {Thread.CurrentThread.ManagedThreadId}");

        }
    }
}
