﻿namespace ConsoleTestApp
{
    delegate int DemoDelegate(int number1, int number2);

    class Program
    {
        static async Task Main(string[] args)
        {
            var demo = new Demo();
            Console.WriteLine("d1");
            DemoDelegate d1 = Sum;
            d1 += demo.Multiply;
            d1(2, 3);
            Console.WriteLine("remove demo.Multiply from d1");
            d1 -= demo.Multiply;
            if (d1 != null) d1(3, 4);

            Console.WriteLine("d2");
            var d2 = new DemoDelegate(Sum);
            d2 = (DemoDelegate)Delegate.Combine(d2, new DemoDelegate(demo.Multiply));
            d2.Invoke(7, 5);
            Console.WriteLine("remove demo.Multiply from d2");
            d2 = (DemoDelegate)Delegate.Remove(d2, new DemoDelegate(demo.Multiply))!;
            d2?.Invoke(6, 8);
            Console.WriteLine("add Sum to d2");
            d2 = (DemoDelegate)Delegate.Combine(d2, new DemoDelegate(Sum));
            d2.Invoke(2, 6);
            Console.WriteLine("remove all demo.Multiply from d2");
            d2 = (DemoDelegate)Delegate.RemoveAll(d2, new DemoDelegate(Sum))!;
            d2?.Invoke(1, 8);

            demo.Event += Sum;
            demo.RaiseEvent(1, 2);
        }

        static int Sum(int number1, int number2)
        {
            var sum = number1 + number2;
            Console.WriteLine($"{number1} + {number2} = {sum}");
            return sum;
        }
    }

    class Demo
    {
        public event DemoDelegate? Event;

        public int Multiply(int number1, int number2)
        {
            var multiply = number1 * number2;
            Console.WriteLine($"{number1} * {number2} = {multiply}");
            return multiply;
        }

        public void RaiseEvent(int number1, int number2)
        {
            Console.WriteLine("Raise Event");
            Event?.Invoke(number1, number2);
        }
    }
}
