﻿using System.Runtime.Intrinsics.X86;
using System.Threading.Channels;

namespace Strings
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(">>> Конкатенация >>>");

            var s1 = "1";
            Console.WriteLine($"s1: {s1}");
            var s2 = "2" + "3";
            Console.WriteLine($"s2: {s2}");
            var s3 = s1 + "2" + "3";
            Console.WriteLine($"s3: {s3}");
            s3 += "4";
            Console.WriteLine($"s3: {s3}");
            var s4 = s1 + s2 + s3;
            Console.WriteLine($"s4: {s4}");
            var s5 = string.Concat(s1, s2, s3, s4);
            Console.WriteLine($"s5: {s5}");
            var s6 = string.Concat(s1, s2, s3, s4, s5);
            Console.WriteLine($"s6: {s6}");
            var s7 = s1 + s2 + s3 + s4 + s5 + s6;
            Console.WriteLine($"s7: {s7}");

            Console.WriteLine(">>> Интернирование >>>");

            var s8 = "1";
            Console.WriteLine($"s8: {s8}");
            Console.WriteLine($"s1 == s8: {s1 == s8}");
            Console.WriteLine($"s1 ReferenceEquals s8: {ReferenceEquals(s1, s8)}");

            var s9 = "1234"; // все литералы интернируются перед началом выполнения программы
            Console.WriteLine($"s9: {s9}");
            Console.WriteLine($"s3 == s9: {s3 == s9}");
            Console.WriteLine($"s3 ReferenceEquals s9: {ReferenceEquals(s3, s9)}");

            Console.WriteLine($"IsInterned s3: {string.IsInterned(s3) ?? "not"}"); // 1234 интернирован, т.к. программа содержит такой литерал, но s3 ссылается другой объект
            Console.WriteLine("Интернируем s3");
            s3 = string.Intern(s3); // ищет в пуле, а если её нет, то создаёт

            var s10 = "1234";
            Console.WriteLine($"s10: {s10}");
            Console.WriteLine($"s3 == s10: {s3 == s10}");
            Console.WriteLine($"s3 ReferenceEquals s10: {ReferenceEquals(s3, s10)}");

            var s11 = "23";
            Console.WriteLine($"s11: {s11}");
            Console.WriteLine($"s2 == s11: {s2 == s11}");
            Console.WriteLine($"s2 ReferenceEquals s11: {ReferenceEquals(s2, s11)}");

            var s12 =  new string("23"); // всегда новый объект, не берёт из пула
            Console.WriteLine($"s12: {s12}");
            Console.WriteLine($"s2 == s12: {s2 == s12}");
            Console.WriteLine($"s2 ReferenceEquals s12: {ReferenceEquals(s2, s12)}");
        }
    }
}